app.factory("CharacterFactory", characterFactory);

const url = "https://www.breakingbadapi.com/api/characters";

function characterFactory($http) {
    return {
        getAllCharacter: getAll
    };

    function getAll(offset, limit) {
        return $http.get(url + "?offset=" + offset + "&limit=" + limit)
            .then(complete)
            .catch(failed);
    }

    function complete(response) {
        return response.data;
    }

    function failed(error) {
        return error;
    }
}