app.controller("CharacterCtrl", characterCtrl);

function characterCtrl(CharacterFactory) {
    const vm = this;
    const offset = 0;
    const limit = 10;

    CharacterFactory.getAllCharacter(offset, limit).then(function (res) {
        vm.characters = res;
    });
}