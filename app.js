var app = angular.module("app", ['ngRoute'])
    .config(routeConfig);

function routeConfig($routeProvider) {
    $routeProvider.when("/", {
        templateUrl: "character/character.html",
        controller: "CharacterCtrl",
        controllerAs: "characterCtrl"
    });
}

